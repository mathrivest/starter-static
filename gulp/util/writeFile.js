'use strict';

var stream = require('stream'),
    gutil = require('gulp-util');

function writeFile(filename, string) {
    var fileStream = stream.Readable({objectMode: true});

    fileStream._read = function () {
        this.push(new gutil.File({
            cwd: '',
            base: '',
            path: filename,
            contents: new Buffer(string)
        }));
        this.push(null);
    };

    return fileStream;
}

module.exports = writeFile;
