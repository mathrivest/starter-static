'use strict';

module.exports = require('connect-modrewrite')([
    '!\\.\\w+$ /index.html [L]'
]);
