'use strict';

var gulp = require('gulp'),
    config = require('../config');


gulp.task('assets:fonts', function () {
    return gulp.src(config.fonts.src)
        .pipe(gulp.dest(config.fonts.dest));
});

gulp.task('assets:images', function () {
    return gulp.src(config.images.src)
        .pipe(gulp.dest(config.images.dest));
});

gulp.task('assets:html', function () {
    return gulp.src(config.layout.src)
        .pipe(gulp.dest(config.layout.dest));
});

gulp.task('assets:scripts', function () {
    return gulp.src(config.scripts.src)
        .pipe(gulp.dest(config.scripts.dest));
});

gulp.task('assets:videos', function () {
    return gulp.src(config.videos.src)
        .pipe(gulp.dest(config.videos.dest));
});
