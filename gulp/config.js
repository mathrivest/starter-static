'use strict';

var pkg = require('../package.json');

var dest = './build',
    src = './src',
    libs = './node_modules';

module.exports = {
    dest: dest,

    changelog: {
        pkg: '../../package.json',
        file: 'CHANGELOG.md',
        repository: pkg.repository.url,
        version: pkg.version,
        subtitle: '',
        from: '',
        to: '',
        issueLink: function (issueId) {
            return pkg.bugs.url + '/browse/' + issueId;
        }
    },

    layout: {
        src: [src + '/*.html'],
        dest: dest
    },

    sass: {
        files: src + '/assets/styles/**/*.scss',
        src: src + '/assets/styles/app.scss',
        minified: 'app.min.css',
        dest: dest,
        sass: {
            errLogToConsole: true,
            includePaths: [libs],
            imagePath: 'images'
        },
        cmq: {
            log: false
        },
        autoprefixer: {
            browsers: ['last 2 version', 'safari 7']
        },
        cssmin: {
            discardComments: {
                removeAll: true
            },
            autoprefixer: {
                browsers: ['last 2 version', 'safari 7']
            }
        },
        sourcemaps: {
            dest: './'
        }
    },

    fonts: {
        src: src + '/assets/fonts/**/*',
        dest: dest + '/fonts'
    },

    scripts: {
        src: src + '/assets/scripts/**/*',
        dest: dest + '/scripts'
    },

    images: {
        src: [src + '/assets/images/**/*'],
        dest: dest + '/images'
    },

    videos: {
        src: [src + '/assets/videos/**/*'],
        dest: dest + '/videos'
    },

    browserSync: {
        open: true,
        notify: false,
        scrollProportionally: false,

        injectChanges: true,
        browser: ['google chrome'],
        port: 9106,

        ui: {
            port: 9103,
            weinre: {
                port: 9104
            }
        },

        server: {
            baseDir: dest
        }
    },

    minification: {
        uglify: {
            mangle: true,
            sourcemap: false,
            report: 'gzip'
        }
    }
};
